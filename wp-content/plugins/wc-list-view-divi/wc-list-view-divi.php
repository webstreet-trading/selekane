<?php

/*

	Plugin Name: WC List View For Divi
	Author: AbdElfatah AboElgit
	Description: Display WooCommerce categories, tags pages and shop page as a list, show product short description, read more button and add to cart button for each product.
	Version: 0.1
	Plugin URI: https://www.divikingdom.com
	Author URI: https://www.divikingdom.com
	License: GNU General Public License v2 or later
	License URI: http://www.gnu.org/licenses/gpl-2.0.html

*/

if( !defined( 'ABSPATH' ) ) exit; // exit if acced directly

define( 'WCLVD_PATH', plugin_dir_path( __FILE__ ) );
define( 'WCLVD_URL', plugin_dir_url( __FILE__ ) );
define( 'WCLVD_NAME', 'WC List View For Divi' );

// the plugin class
if( !class_exists( 'WCLVD_Plugin' ) ){
	class WCLVD_Plugin{

		public function __construct(){

			// Hooks
			add_action( 'admin_menu', array( $this, 'settings_menu_page' ) );
			add_action( 'admin_init', array( $this, 'register_plugin_settings' ) );
			add_action( 'wp', array( $this, 'load_frontend' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'frontend_scripts' ) );

		}

		// check if Divi is active
		public function is_divi_active(){
			$current_theme = wp_get_theme();

			if( $current_theme->Name == 'Divi' || $current_theme->Template == 'Divi' || $current_theme->Template == 'divi' ){
				return true;
			}else{
				return false;
			}
		}

		// plugin settings menu page
		public function settings_menu_page(){
			add_options_page( WCLVD_NAME, 'WC List View', 'manage_options', 'wclvd', array( $this, 'settings_page_content' ) );
		}

		// settings page content
		public function settings_page_content(){
			require WCLVD_PATH . 'admin/index.php';
		}

		// register plugin settings
		public function register_plugin_settings(){
			register_setting( 'wclvd_settings', 'wclvd_settings', 
				array( 
					'sanitize_callback' => array( $this, 'sanitize_settings' ) 
				) 
			);
		}

		// get plugin settings
		public static function get_settings(){
			return get_option( 'wclvd_settings' );
		}

		public function sanitize_settings( $inputs ){

			$sanitized_settings = array(
				'shop_page' => array(
					'enable_list_view' 	=> $inputs['shop_page']['enable_list_view'] == 1 ? 1 : 0,
					'show_excerpt' 		=> $inputs['shop_page']['show_excerpt'] == 1 ? 1 : 0,
					'show_read_more' 	=> $inputs['shop_page']['show_read_more'] == 1 ? 1 : 0,
					'read_more_text'	=> sanitize_text_field( $inputs['shop_page']['read_more_text'] ),
					'show_add_to_cart' 	=> $inputs['shop_page']['show_add_to_cart'] == 1 ? 1 : 0
				),
				'cat_page' => array(
					'enable_list_view' 	=> $inputs['cat_page']['enable_list_view'] == 1 ? 1 : 0,
					'show_excerpt' 		=> $inputs['cat_page']['show_excerpt'] == 1 ? 1 : 0,
					'show_read_more' 	=> $inputs['cat_page']['show_read_more'] == 1 ? 1 : 0,
					'read_more_text'	=> sanitize_text_field( $inputs['cat_page']['read_more_text'] ),
					'show_add_to_cart' 	=> $inputs['cat_page']['show_add_to_cart'] == 1 ? 1 : 0,
					'enable_all_cats' 	=> $inputs['cat_page']['enable_all_cats'] == 1 ? 1 : 0,
					'cats_ids'			=> (array)$inputs['cat_page']['cats_ids']
				),
				'tag_page' => array(
					'enable_list_view' 	=> $inputs['tag_page']['enable_list_view'] == 1 ? 1 : 0,
					'show_excerpt' 		=> $inputs['tag_page']['show_excerpt'] == 1 ? 1 : 0,
					'show_read_more' 	=> $inputs['tag_page']['show_read_more'] == 1 ? 1 : 0,
					'read_more_text'	=> sanitize_text_field( $inputs['tag_page']['read_more_text'] ),
					'show_add_to_cart' 	=> $inputs['tag_page']['show_add_to_cart'] == 1 ? 1 : 0,
					'enable_all_tags' 	=> $inputs['tag_page']['enable_all_tags'] == 1 ? 1 : 0,
					'tags_ids'			=> (array)$inputs['tag_page']['tags_ids']
				),
				'styling' => array(
					'read_more' => array(
						'background' 		=> sanitize_hex_color( $inputs['styling']['read_more']['background'] ),
						'text_color'		=> sanitize_hex_color( $inputs['styling']['read_more']['text_color'] ),
						'border_width'		=> is_numeric( $inputs['styling']['read_more']['border_width'] ) ? $inputs['styling']['read_more']['border_width'] : 2,
						'border_color'		=> sanitize_hex_color( $inputs['styling']['read_more']['border_color'] ),

						'hvr_background'	=> sanitize_hex_color( $inputs['styling']['read_more']['hvr_background'] ),
						'hvr_text_color'	=> sanitize_hex_color( $inputs['styling']['read_more']['hvr_text_color'] ),
						'hvr_border_width'	=> is_numeric( $inputs['styling']['read_more']['hvr_border_width'] ) ? $inputs['styling']['read_more']['hvr_border_width'] : 2,
						'hvr_border_color'	=> sanitize_hex_color( $inputs['styling']['read_more']['hvr_border_color'] ),
					),
					'add_to_cart' => array(
						'background' 		=> sanitize_hex_color( $inputs['styling']['add_to_cart']['background'] ),
						'text_color'		=> sanitize_hex_color( $inputs['styling']['add_to_cart']['text_color'] ),
						'border_width'		=> is_numeric( $inputs['styling']['add_to_cart']['border_width'] ) ? $inputs['styling']['add_to_cart']['border_width'] : 2,
						'border_color'		=> sanitize_hex_color( $inputs['styling']['add_to_cart']['border_color'] ),

						'hvr_background'	=> sanitize_hex_color( $inputs['styling']['add_to_cart']['hvr_background'] ),
						'hvr_text_color'	=> sanitize_hex_color( $inputs['styling']['add_to_cart']['hvr_text_color'] ),
						'hvr_border_width'	=> is_numeric( $inputs['styling']['add_to_cart']['hvr_border_width'] ) ? $inputs['styling']['add_to_cart']['hvr_border_width'] : 2,
						'hvr_border_color'	=> sanitize_hex_color( $inputs['styling']['add_to_cart']['hvr_border_color'] ),
					),
				)
			);

			return $sanitized_settings;
		}

		// admin scripts
		public function admin_scripts(){
			wp_enqueue_style( 'wclvd-admin-css', WCLVD_URL . 'admin/css/style.css' );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_script( 'wclvd-admin-js', WCLVD_URL . 'admin/js/main.js', array( 'jquery', 'wp-color-picker' ), false, true );
		}

		// frontend scripts
		public function frontend_scripts(){
			// only load this script if the body has wclvd_list class
			$classes = get_body_class();
			if( in_array( 'wclvd_list', $classes ) ){
				wp_enqueue_style( 'wclvd-css', WCLVD_URL . 'frontend/css/style.css' );
			}
		}

		// frontend styling
		public function load_frontend(){

			// return if Divi is not active
			if( !$this->is_divi_active() ){
				return;
			}

			$settings = self::get_settings();

			// shop page
			if( function_exists( 'is_shop' ) && is_shop() ){

				// list view body class
				if( $settings['shop_page']['enable_list_view'] == 1 ){
					$this->add_body_class();
				}

				// product excerpt
				if( $settings['shop_page']['show_excerpt'] == 1 ){
					$this->get_product_excerpt();
				}

				// buttons
				if( $settings['shop_page']['show_read_more'] == 1 || $settings['shop_page']['show_add_to_cart'] == 1 ){

					// output buttons HTML wrapper start
					add_action( 'woocommerce_after_shop_loop_item', array( $this, 'buttons_html_wrapper_start' ), 8 );

					// output read more button
					if( $settings['shop_page']['show_read_more'] == 1 ){

						$this->button_style( 'read_more' );

						$link_text = $settings['shop_page']['read_more_text'];
						$this->get_read_more( $link_text );
					}

					// output add to cart button
					if( $settings['shop_page']['show_add_to_cart'] == 1 ){

						$this->button_style( 'add_to_cart' );
						$this->get_add_to_cart();
					}	

					// output buttons HTML wrapper end
					add_action( 'woocommerce_after_shop_loop_item', array( $this, 'buttons_html_wrapper_end' ), 11 );				
				}

			}

			// categories pages
			if( function_exists( 'is_product_category' ) && is_product_category() ){

				if( $settings['cat_page']['enable_list_view'] == 1 ){

					$current_cat = get_queried_object_id();
					if( in_array( $current_cat, $settings['cat_page']['cats_ids'] ) || $settings['cat_page']['enable_all_cats'] == 1 ){
						$this->add_body_class();
					}
					
				}

				if( $settings['cat_page']['show_excerpt'] == 1 ){
					$this->get_product_excerpt();
				}

				// buttons
				if( $settings['cat_page']['show_read_more'] == 1 || $settings['cat_page']['show_add_to_cart'] == 1 ){

					// output buttons HTML wrapper start
					add_action( 'woocommerce_after_shop_loop_item', array( $this, 'buttons_html_wrapper_start' ), 8 );

					// output read more button
					if( $settings['cat_page']['show_read_more'] == 1 ){
						$this->button_style( 'read_more' );

						$link_text = $settings['cat_page']['read_more_text'];
						$this->get_read_more( $link_text );
					}

					// output add to cart button
					if( $settings['cat_page']['show_add_to_cart'] == 1 ){
						$this->button_style( 'add_to_cart' );
						$this->get_add_to_cart();
					}	

					// output buttons HTML wrapper end
					add_action( 'woocommerce_after_shop_loop_item', array( $this, 'buttons_html_wrapper_end' ), 11 );				
				}
			}

			// tags pages
			if( function_exists( 'is_product_tag' ) && is_product_tag() ){
				
				// show list view
				if( $settings['tag_page']['enable_list_view'] == 1 ){

					$current_tag = get_queried_object_id();
					if( in_array( $current_tag, $settings['tag_page']['tags_ids'] ) || $settings['tag_page']['enable_all_tags'] == 1 ){
						$this->add_body_class();
					}
				}

				if( $settings['tag_page']['show_excerpt'] == 1 ){
					$this->get_product_excerpt();
				}

				// buttons
				if( $settings['tag_page']['show_read_more'] == 1 || $settings['tag_page']['show_add_to_cart'] == 1 ){

					// output buttons HTML wrapper start
					add_action( 'woocommerce_after_shop_loop_item', array( $this, 'buttons_html_wrapper_start' ), 8 );

					// output read more button
					if( $settings['tag_page']['show_read_more'] == 1 ){
						$this->button_style( 'read_more' );

						$link_text = $settings['tag_page']['read_more_text'];
						$this->get_read_more( $link_text );
					}

					// output add to cart button
					if( $settings['tag_page']['show_add_to_cart'] == 1 ){
						$this->button_style( 'add_to_cart' );
						$this->get_add_to_cart();
					}	

					// output buttons HTML wrapper end
					add_action( 'woocommerce_after_shop_loop_item', array( $this, 'buttons_html_wrapper_end' ), 11 );				
				}
			}

		}

		// add body class
		public function add_body_class(){
			add_filter( 'body_class', function( $classes ){
				$classes[] = 'wclvd_list';
				return $classes;
			} );			
		}

		// product excerpt
		public function get_product_excerpt(){
			// close the product link
			add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 10 );

			// excerpt
			add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_single_excerpt', 20 );			
		}

		// add to cart button
		public function get_add_to_cart(){
			add_action( 'woocommerce_after_shop_loop_item', function(){
				woocommerce_template_loop_add_to_cart();
			}, 10 );
		}

		// get all product tags/categories
		public static function get_products_terms( $taxonomy = 'product_cat' ){

			$terms = get_terms( array(

				'taxonomy' => $taxonomy,
				'hide_empty' => false

			) );
			return (array)$terms;
		}

		// read more button
		public function get_read_more( $link ){
			global $link_text;
			$link_text = !empty( $link ) ? esc_attr( $link ) : "Read More";

			add_action( 'woocommerce_after_shop_loop_item', function(){
					global $link_text;
					echo "<a href='". get_permalink() ."' class='button wclvd_read_more'>{$link_text}</a>";
				}, 9 );
		}

		// buttons_html_wrapper_start
		public function buttons_html_wrapper_start(){
			echo "<div class='wclvd_btns'>";
		}

		// buttons_html_wrapper_end
		public function buttons_html_wrapper_end(){
			echo "</div>";
		}

		// output buttons style
		public function button_style( $button ){
			$settings = self::get_settings();

			if( isset( $settings['styling'] ) ){

				// read more button CSS
				if( $button == 'read_more' ){

					global $read_more_styling;
					$read_more_styling = $settings['styling']['read_more'];

					add_action( 'wp_head', function(){
						global $read_more_styling;
						?>
						<style>
							.woocommerce-page .wclvd_btns .button.wclvd_read_more{
								<?php 
									echo !empty( $read_more_styling['background'] ) ? "background:" . esc_attr( $read_more_styling['background'] ) . " !important;" : '';
									echo !empty( $read_more_styling['text_color'] ) ? "color:" . esc_attr( $read_more_styling['text_color'] ) . " !important;" : '';
									echo (int)$read_more_styling['border_width'] >= 0 ? "border-width:" . esc_attr( $read_more_styling['border_width'] ) . "px !important;" : '';
									echo !empty( $read_more_styling['border_color'] ) ? "border-color:" . esc_attr( $read_more_styling['border_color'] ) . " !important;" : ''; 
								?>
							}
							.woocommerce-page .wclvd_btns .button.wclvd_read_more:hover{
								<?php 
									echo !empty( $read_more_styling['hvr_background'] ) ? "background:" . esc_attr( $read_more_styling['hvr_background'] ) . " !important;" : '';
									echo !empty( $read_more_styling['hvr_text_color'] ) ? "color:" . esc_attr( $read_more_styling['hvr_text_color'] ) . " !important;" : '';
									echo (int)$read_more_styling['hvr_border_width'] >= 0 ? "border-width:" . esc_attr( $read_more_styling['hvr_border_width'] ) . "px !important;" : '';
									echo !empty( $read_more_styling['hvr_border_color'] ) ? "border-color:" . esc_attr( $read_more_styling['hvr_border_color'] ) . " !important;" : ''; 
								?>
							}
						</style>
						<?php
					} );			
				}

				// add to cart button CSS
				if( $button == 'add_to_cart' ){

					global $add_to_cart_styling;
					$add_to_cart_styling = $settings['styling']['add_to_cart'];

					add_action( 'wp_head', function(){
						global $add_to_cart_styling;
						?>
						<style>
							.woocommerce-page .wclvd_btns .button.add_to_cart_button,
							.woocommerce-page .wclvd_btns .button.product_type_simple{
								<?php echo !empty( $add_to_cart_styling['background'] ) ? "background:" . esc_attr( $add_to_cart_styling['background'] ) . " !important;" : ''; ?>
								<?php echo !empty( $add_to_cart_styling['text_color'] ) ? "color:" . esc_attr( $add_to_cart_styling['text_color'] ) . " !important;" : ''; ?>
								<?php echo (int)$add_to_cart_styling['border_width'] >= 0 ? "border-width:" . esc_attr( $add_to_cart_styling['border_width'] ) . "px !important;" : ''; ?>
								<?php echo !empty( $add_to_cart_styling['border_color'] ) ? "border-color:" . esc_attr( $add_to_cart_styling['border_color'] ) . " !important;" : ''; ?>
							}
							.woocommerce-page .wclvd_btns .button.add_to_cart_button:hover,
							.woocommerce-page .wclvd_btns .button.product_type_simple:hover{
								<?php 
									echo !empty( $add_to_cart_styling['hvr_background'] ) ? "background:" . esc_attr( $add_to_cart_styling['hvr_background'] ) . " !important;" : '';
									echo !empty( $add_to_cart_styling['hvr_text_color'] ) ? "color:" . esc_attr( $add_to_cart_styling['hvr_text_color'] ) . " !important;" : '';
									echo (int)$add_to_cart_styling['hvr_border_width'] >= 0 ? "border-width:" . esc_attr( $add_to_cart_styling['hvr_border_width'] ) . "px !important;" : '';
									echo !empty( $add_to_cart_styling['hvr_border_color'] ) ? "border-color:" . esc_attr( $add_to_cart_styling['hvr_border_color'] ) . " !important;" : ''; 
								?>
							}
						</style>
						<?php
					} );			
				}

			}
		}

	}
}

// initializing the class
function wclvd_init(){
	global $wclvd;
	$wclvd = new WCLVD_Plugin();
}
wclvd_init();
