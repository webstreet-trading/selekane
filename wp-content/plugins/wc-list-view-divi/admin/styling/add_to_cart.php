<section class="add_to_cart_styling">
	
	<?php 
		$add_to_cart = @$plugin_settings['styling']['add_to_cart']; 
	?>

	<h2>Add To Cart Button</h2>
	<table class="form-table">

		<tr>
			<th scope="row">Background Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][add_to_cart][background]" id="wclvd_rm_bg" value="<?php echo esc_attr( $add_to_cart['background'] ); ?>">
			</td>
		</tr>

		<tr>
			<th scope="row">Text Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][add_to_cart][text_color]" id="wclvd_rm_text_color" value="<?php echo esc_attr( $add_to_cart['text_color'] ); ?>">
			</td>
		</tr>

		<tr>
			<th scope="row">Border Width</th>
			<td>
				<input type="number" min="0" max="10" step="1" name="wclvd_settings[styling][add_to_cart][border_width]" value="<?php echo esc_attr( $add_to_cart['border_width'] ); ?>"> px
			</td>
		</tr>

		<tr>
			<th scope="row">Border Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][add_to_cart][border_color]" value="<?php echo esc_attr( $add_to_cart['border_color'] ); ?>">
			</td>
		</tr>

		<tr>
			<td colspan="2"><h2>On Hover</h2></td>
		</tr>	

		<tr>
			<th scope="row">Background Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][add_to_cart][hvr_background]" id="wclvd_rm_hvr_bg" value="<?php echo esc_attr( $add_to_cart['hvr_background'] ); ?>">
			</td>
		</tr>

		<tr>
			<th scope="row">Text Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][add_to_cart][hvr_text_color]" id="wclvd_rm_hvr_text_color" value="<?php echo esc_attr( $add_to_cart['hvr_text_color'] ); ?>">
			</td>
		</tr>

		<tr>
			<th scope="row">Border Width</th>
			<td>
				<input type="number" min="0" max="10" step="1" name="wclvd_settings[styling][add_to_cart][hvr_border_width]" value="<?php echo esc_attr( $add_to_cart['hvr_border_width'] ); ?>"> px
			</td>
		</tr>

		<tr>
			<th scope="row">Border Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][add_to_cart][hvr_border_color]" value="<?php echo esc_attr( $add_to_cart['hvr_border_color'] ); ?>">
			</td>
		</tr>

	</table>
</section>