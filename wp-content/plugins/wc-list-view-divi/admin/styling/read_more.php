<section class="read_more_styling">
	
	<?php 
		$read_more = @$plugin_settings['styling']['read_more']; 
	?>

	<h2>Read More Button</h2>
	<table class="form-table">

		<tr>
			<th scope="row">Background Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][read_more][background]" value="<?php echo esc_attr( $read_more['background'] ); ?>">
			</td>
		</tr>

		<tr>
			<th scope="row">Text Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][read_more][text_color]" value="<?php echo esc_attr( $read_more['text_color'] ); ?>">
			</td>
		</tr>

		<tr>
			<th scope="row">Border Width</th>
			<td>
				<input type="number" min="0" max="10" step="1" name="wclvd_settings[styling][read_more][border_width]" value="<?php echo esc_attr( $read_more['border_width'] ); ?>"> px
			</td>
		</tr>

		<tr>
			<th scope="row">Border Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][read_more][border_color]" value="<?php echo esc_attr( $read_more['border_color'] ); ?>">
			</td>
		</tr>

		<tr>
			<td colspan="2"><h2>On Hover</h2></td>
		</tr>	

		<tr>
			<th scope="row">Background Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][read_more][hvr_background]" value="<?php echo esc_attr( $read_more['hvr_background'] ); ?>">
			</td>
		</tr>

		<tr>
			<th scope="row">Text Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][read_more][hvr_text_color]" value="<?php echo esc_attr( $read_more['hvr_text_color'] ); ?>">
			</td>
		</tr>

		<tr>
			<th scope="row">Border Width</th>
			<td>
				<input type="number" min="0" max="10" step="1" name="wclvd_settings[styling][read_more][hvr_border_width]" value="<?php echo esc_attr( $read_more['hvr_border_width'] ); ?>"> px
			</td>
		</tr>

		<tr>
			<th scope="row">Border Color</th>
			<td>
				<input type="text" class="color-field" name="wclvd_settings[styling][read_more][hvr_border_color]" value="<?php echo esc_attr( $read_more['hvr_border_color'] ); ?>">
			</td>
		</tr>

	</table>
</section>