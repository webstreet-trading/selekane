<section class="cat_page">
	<h2>Categories Pages</h2>
	<table class="form-table">

		<tr>
			<th scope="row">Enable List View</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" class="wclvd_show_toggle" data-show="choose_cats" value="1" id="cat_enable_list_view" name="wclvd_settings[cat_page][enable_list_view]" <?php echo @$plugin_settings['cat_page']['enable_list_view'] == 1 ? 'checked' : ''; ?>>
					<label for="cat_enable_list_view"></label>
				</div>
			</td>
		</tr>
		
		<tr <?php echo @$plugin_settings['cat_page']['enable_list_view'] != 1 ? 'class="wclvd_hidden"' : ''; ?> id="choose_cats">
			<th scope="row">Choose Categories</th>
			<td>
				<?php if( count( $product_cats ) ){ ?>
						<p style="margin-bottom: 20px">
							<label>
								<input type="checkbox" class="select_deselect_cats" name="wclvd_settings[cat_page][enable_all_cats]" value="1"> <b>Select All</b>
							</label>
						</p>

						
						<?php foreach( $product_cats as $cat ){ ?>
							<label class="cat_label">
								<input type="checkbox" class="cat_checkbox" name="wclvd_settings[cat_page][cats_ids][]" value="<?php echo $cat->term_id ?>" <?php if( in_array( $cat->term_id, (array)$plugin_settings['cat_page']['cats_ids'] ) || @$plugin_settings['cat_page']['enable_all_cats'] == 1 ){ echo "checked"; } ?>> <?php echo $cat->name; ?>
							</label>
						<?php }
					
					} 
				?>
			</td>
		</tr>

		<tr>
			<th scope="row">Show Short Description</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" id="cat_show_excerpt" name="wclvd_settings[cat_page][show_excerpt]" <?php echo @$plugin_settings['cat_page']['show_excerpt'] == 1 ? 'checked' : ''; ?>>
					<label for="cat_show_excerpt"></label>
				</div>
			</td>
		</tr>

		<tr>
			<th scope="row">Show Read More Button</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" id="cat_show_read_more" class="wclvd_show_toggle" data-show="cat_read_more_text" name="wclvd_settings[cat_page][show_read_more]" <?php echo @$plugin_settings['cat_page']['show_read_more'] == 1 ? 'checked' : ''; ?> >
					<label for="cat_show_read_more"></label>
				</div>
			</td>
		</tr>

		<tr <?php echo @$plugin_settings['cat_page']['show_read_more'] != 1 ? 'class="wclvd_hidden"' : ''; ?> id="cat_read_more_text">
			<th scope="row">Read More Button Text</th>
			<td>
				<input type="text" name="wclvd_settings[cat_page][read_more_text]" value="<?php echo esc_attr( @$plugin_settings['cat_page']['read_more_text'] ); ?>">
				<p class="description">If left empty, "Read More" will be used.</p>
			</td>
		</tr>

		<tr>
			<th scope="row">Show Add To Cart Button</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" id="cat_show_add_to_cart" name="wclvd_settings[cat_page][show_add_to_cart]" <?php echo @$plugin_settings['cat_page']['show_add_to_cart'] == 1 ? 'checked' : ''; ?>>
					<label for="cat_show_add_to_cart"></label>
				</div>
			</td>
		</tr>

	</table>			
</section> <!-- cat_page -->