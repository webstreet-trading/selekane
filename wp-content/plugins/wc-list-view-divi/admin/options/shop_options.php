<section class="shop_page">
	<h2>Shop Page</h2>
	<table class="form-table">

		<tr>
			<th scope="row">Enable List View</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" id="shop_enable_list_view" name="wclvd_settings[shop_page][enable_list_view]" <?php echo @$plugin_settings['shop_page']['enable_list_view'] == 1 ? 'checked' : ''; ?>>
					<label for="shop_enable_list_view"></label>
				</div>
			</td>
		</tr>

		<tr>
			<th scope="row">Show Short Description</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" id="shop_show_excerpt" name="wclvd_settings[shop_page][show_excerpt]" <?php echo @$plugin_settings['shop_page']['show_excerpt'] == 1 ? 'checked' : ''; ?>>
					<label for="shop_show_excerpt"></label>
				</div>
			</td>
		</tr>

		<tr>
			<th scope="row">Show Read More Button</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" id="shop_show_read_more" class="wclvd_show_toggle" data-show="shop_read_more_text" name="wclvd_settings[shop_page][show_read_more]" <?php echo @$plugin_settings['shop_page']['show_read_more'] == 1 ? 'checked' : ''; ?> >
					<label for="shop_show_read_more"></label>
				</div>
			</td>
		</tr>

		<tr <?php echo @$plugin_settings['shop_page']['show_read_more'] != 1 ? 'class="wclvd_hidden"' : ''; ?> id="shop_read_more_text">
			<th scope="row">Read More Button Text</th>
			<td>
				<input type="text" name="wclvd_settings[shop_page][read_more_text]" value="<?php echo esc_attr( @$plugin_settings['shop_page']['read_more_text'] ); ?>">
				<p class="description">If left empty, "Read More" will be used.</p>
			</td>
		</tr>

		<tr>
			<th scope="row">Show Add To Cart Button</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" id="shop_show_add_to_cart" name="wclvd_settings[shop_page][show_add_to_cart]" <?php echo @$plugin_settings['shop_page']['show_add_to_cart'] == 1 ? 'checked' : ''; ?> >
					<label for="shop_show_add_to_cart"></label>
				</div>
			</td>
		</tr>

	</table>			
</section> <!-- shop_page -->