<section class="tag_page">
	<h2>Tags Pages</h2>
	<table class="form-table">

		<tr>
			<th scope="row">Enable List View</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" class="wclvd_show_toggle" data-show="choose_tags" id="tag_enable_list_view" name="wclvd_settings[tag_page][enable_list_view]" <?php echo @$plugin_settings['tag_page']['enable_list_view'] == 1 ? 'checked' : ''; ?> >
					<label for="tag_enable_list_view"></label>
				</div>
			</td>
		</tr>

		<tr <?php echo @$plugin_settings['tag_page']['enable_list_view'] != 1 ? 'class="wclvd_hidden"' : ''; ?> id="choose_tags">
			<th scope="row">Choose Tags</th>
			<td>
				<?php if( count( $product_tags ) ){ ?>
						<p style="margin-bottom: 20px">
							<label>
								<input type="checkbox" class="select_deselect_tags" name="wclvd_settings[tag_page][enable_all_tags]" value="1"> <b>Select All</b>
							</label>
						</p>
						
						<?php foreach( $product_tags as $tag ){ ?>
							<label class="tag_label">
								<input type="checkbox" class="tag_checkbox" name="wclvd_settings[tag_page][tags_ids][]" value="<?php echo $tag->term_id ?>" <?php if( in_array( $tag->term_id, (array)$plugin_settings['tag_page']['tags_ids'] ) || @$plugin_settings['tag_page']['enable_all_tags'] == 1 ){ echo "checked"; } ?>> <?php echo $tag->name; ?>
							</label>
						<?php }
					
					} 
				?>
			</td>
		</tr>				

		<tr>
			<th scope="row">Show Short Description</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" id="tag_show_excerpt" name="wclvd_settings[tag_page][show_excerpt]" <?php echo @$plugin_settings['tag_page']['show_excerpt'] == 1 ? 'checked' : ''; ?> >
					<label for="tag_show_excerpt"></label>
				</div>
			</td>
		</tr>

		<tr>
			<th scope="row">Show Read More Button</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" id="tag_show_read_more" class="wclvd_show_toggle" data-show="tag_read_more_text" name="wclvd_settings[tag_page][show_read_more]" <?php echo @$plugin_settings['tag_page']['show_read_more'] == 1 ? 'checked' : ''; ?> >
					<label for="tag_show_read_more"></label>
				</div>
			</td>
		</tr>

		<tr <?php echo @$plugin_settings['tag_page']['show_read_more'] != 1 ? 'class="wclvd_hidden"' : ''; ?> id="tag_read_more_text">
			<th scope="row">Read More Button Text</th>
			<td>
				<input type="text" name="wclvd_settings[tag_page][read_more_text]" value="<?php echo esc_attr( @$plugin_settings['tag_page']['read_more_text'] ); ?>">
				<p class="description">If left empty, "Read More" will be used.</p>
			</td>
		</tr>

		<tr>
			<th scope="row">Show Add To Cart Button</th>
			<td>
				<div class="dk_checkbox">	
					<input type="checkbox" value="1" id="tag_show_add_to_cart" name="wclvd_settings[tag_page][show_add_to_cart]" <?php echo @$plugin_settings['tag_page']['show_add_to_cart'] == 1 ? 'checked' : ''; ?> >
					<label for="tag_show_add_to_cart"></label>
				</div>
			</td>
		</tr>

	</table>			
</section> <!-- cat_page -->		