<?php if( !defined( 'ABSPATH' ) ) exit; ?>

<div class="wrap wclvd_settings">
	<h1 style="margin-bottom: 20px;"><?php echo WCLVD_NAME . ' Settings'; ?></h1>

	<form method="post" action="options.php">
		<?php 
			settings_fields( 'wclvd_settings' );
			$plugin_settings 	= WCLVD_Plugin::get_settings();
			$product_cats 		= WCLVD_Plugin::get_products_terms();
			$product_tags 		= WCLVD_Plugin::get_products_terms( 'product_tag' );
		?>

		<div class="tabs_header">
			<a href="#options" class="active" data-show="#options">Options</a>
			<a href="#styling" data-show="#styling">Styling</a>
		</div>

		<div class="tabs_body">

			<div id="options">
				<?php 
					require WCLVD_PATH . 'admin/options/shop_options.php'; 
					require WCLVD_PATH . 'admin/options/cats_options.php'; 
					require WCLVD_PATH . 'admin/options/tags_options.php';
				?>				
			</div><!-- #options -->

			<div id="styling">
				<?php
					require WCLVD_PATH . 'admin/styling/read_more.php'; 
					require WCLVD_PATH . 'admin/styling/add_to_cart.php'; 
				?>				
			</div><!-- #styling -->

			<p class="submit">
				<input type="submit" name="submit" value="Save!" class="button button-primary dk_button">
			</p>

		</div>

		<div class="dk_promo">
			<h2>Join Our Mailing List</h2>
			<div class="box_inner">
				<p>Subscribe to our newsletter now and never miss a freebie. We offer great Divi plugins, layouts and tutorials.</p>
				<p><a href="https://www.divikingdom.com/newsletter-signup/" target="_blank" class="dk_btn">Subscribe Now!</a></p>
			</div><!-- /.box_inner -->
		</div><!-- /.dk_promo -->

		<div class="dk_promo get_in_touch">
			<h2>Get In Touch</h2>
			<div class="box_inner">
				<p>Have a question? Contact Me Now!</p>
				<p class="contact_info">
					<a href="https://www.facebook.com/DiviKingdom/" target="_blank" title="Our Facebook Page">
						<span class="dashicons dashicons-facebook-alt"></span>
					</a>
					<a href="https://twitter.com/SailorAbood" target="_blank" title="My Twitter Page">
						<span class="dashicons dashicons-twitter"></span>
					</a>
					<a href="https://www.divikingdom.com/contact/" target="_blank" title="Contact Us">
						<span class="dashicons dashicons-email-alt"></span>
					</a>
				</p>
			</div><!-- /.box_inner -->
		</div><!-- /.dk_promo -->

		<div class="clear"></div><!-- /.clear -->
	</form>
</div><!-- /.wrap -->