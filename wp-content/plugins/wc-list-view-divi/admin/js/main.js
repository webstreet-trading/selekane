jQuery(document).ready(function($){

	// admin tabs
	$('.wclvd_settings .tabs_header a').on( 'click', function(e){
		e.preventDefault();

		$(this).siblings().removeClass('active');
		$(this).addClass('active');

		var show_tab = $( $(this).data('show') ),
			hide_tab = $( $(this).siblings().data('show') );

		hide_tab.fadeOut();
		show_tab.fadeIn();
	} );

	// show or hide something if a checkbox is checked
	$('.wclvd_show_toggle').on('change',function(){

		var toggle_element = $("#" + $(this).data('show'));

		if( $(this).is(":checked") ){
			toggle_element.fadeIn();
		}else{
			toggle_element.fadeOut();
		}
	});

	// select/deselect all cats
	$('.select_deselect_cats').on('click',function(){
		if( $(this).is(":checked") ){
			$('.cat_checkbox').attr('checked','checked');
		}else{
			$('.cat_checkbox').removeAttr('checked');
		}
	});

	// deselect toggle chckbox if some cats are deselected
	if( $('.cat_checkbox').length == $('.cat_checkbox:checked').length ){
		$('.select_deselect_cats').attr('checked','checked');
	}

	$('.cat_checkbox').on('change',function(){

		// select toggle checkbox if all terms are selected
		if( $('.cat_checkbox').length == $('.cat_checkbox:checked').length ){
			$('.select_deselect_cats').attr('checked','checked');
		}else{
			$('.select_deselect_cats').removeAttr('checked');
		}

	});

	// select/deselect all tags
	$('.select_deselect_tags').on('click',function(){
		if( $(this).is(":checked") ){
			$('.tag_checkbox').attr('checked','checked');
		}else{
			$('.tag_checkbox').removeAttr('checked');
		}
	});

	// deselect toggle chckbox if some tags are deselected
	if( $('.tag_checkbox').length == $('.tag_checkbox:checked').length ){
		$('.select_deselect_tags').attr('checked','checked');
	}
	
	$('.tag_checkbox').on('change',function(){

		// select toggle checkbox if all terms are selected
		if( $('.tag_checkbox').length == $('.tag_checkbox:checked').length ){
			$('.select_deselect_tags').attr('checked','checked');
		}else{
			$('.select_deselect_tags').removeAttr('checked');
		}

	});	

	// color palette
	$('.color-field').wpColorPicker();

});