<?php

// run includes
include('ws/divi/functions.php');

/**
 * Set styles and javascript
 */
function ws_enqueue_css_js() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('theme-style', get_stylesheet_directory_uri() . '/assets/css/styles.css', array(), ws('version'));
    
    wp_enqueue_script('theme-js', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery'), ws('version'), true);
}
add_action('wp_enqueue_scripts', 'ws_enqueue_css_js', 99);

/**
 * Add custom tabs Filters ePanel's localized tab names.
 *
 * @param string[] $tabs {
 *
 *     @type string $tab_slug Localized tab name.
 *     ...
 * }
 */
function ws_et_epanel_tab_names($tabs) {
    
    // add ws tab
    $tabs['ws'] = __('WS', 'ws');
    
    return $tabs;
}
add_filter('et_epanel_tab_names', 'ws_et_epanel_tab_names');

function ws_et_epanel_layout_data($options) {
    global $shortname;
    
    // add mailchimp options
    $ws_settings_options = [
        
        // new tab
        [
            'name' => 'wrap-ws',
            'type' => 'contenttab-wrapstart',
            'desc' => esc_html__('WS', 'ws'),
        ],
        ['type' => 'subnavtab-start'],
        [
            'name' => 'ws-1',
            'type' => 'subnav-tab',
            'desc' => esc_html__('Footer Details', 'ws'),
        ],
        ['type' => 'subnavtab-end'],
        [
            'name' => 'ws-1',
            'type' => 'subcontent-start',
        ],
        
        // options
        [
            "name" => esc_html__("Footer Address", $themename),
            "id" => $shortname . "_footer_address",
            "type" => "text",
            "desc" => esc_html__("Input the address that will display in the site's footer.", $themename),
            "validation_type" => "text"
        ],
        [
            "name" => esc_html__("Footer Telephone Number", $themename),
            "id" => $shortname . "_footer_tel",
            "type" => "text",
            "desc" => esc_html__("Input the telephone number that will display in the site's footer.", $themename),
            "validation_type" => "text"
        ],
        [
            "name" => esc_html__("Footer Email Address", $themename),
            "id" => $shortname . "_footer_email",
            "type" => "text",
            "desc" => esc_html__("Input the email address that will display in the site's footer.", $themename),
            "validation_type" => "email"
        ], 
        
        // footer
        [
            'name' => 'ws-1',
            'type' => 'subcontent-end',
        ],
        [
            'name' => 'wrap-ws',
            'type' => 'contenttab-wrapend',
        ],
    ];
    
    return array_merge($options, $ws_settings_options);
}
add_filter('et_epanel_layout_data', 'ws_et_epanel_layout_data');


/**
 * add shortcode date
 */
function ws_shortcode_date($atts = []) {
    $atts = shortcode_atts([
        'format' => 'Y',
            ], $atts);

    return date($atts['format']);
}
add_shortcode('date', 'ws_shortcode_date');


/**
 * Output shortcode for footer credits
 * @return void
 */
function ws_get_footer_credits() {
    echo do_shortcode(et_get_footer_credits());
}