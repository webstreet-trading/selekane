<div class="footer_contact_contain">
    <div class="footer_contact_inner">
        <p class="footer_contact_line">
            <span class="footer_contact_item footer_contact_address">
                <a href="#">Corner House 5th Floor, Office 1, 77 Commissioner Street, JHB.</a>
            </span>
        </p>
        
        <p class="footer_contact_line">
            <span class="footer_contact_item footer_contact_tel">
                <a href="#">+27 11 492 0006</a>
            </span>
            <span class="footer_contact_item footer_contact_email">
                <a href="#">info@billyselekane.com</a>
            </span>
        </p>
        
        <p class="footer_contact_line">
            <span class="footer_contact_item footer_contact_copy">
                <a href="#">© 2018 Billy Selekane International. All Rights Reserved.</a>
            </span>
        </p>
    </div>
</div>